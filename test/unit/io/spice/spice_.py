# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
# type: ignore
from textwrap import dedent
import unittest

from pdkmaster.design import cell as _cell
from pdkmaster.io import spice as _sp

from ...dummy import (
    dummy_tech, dummy_cktfab, dummy_layoutfab, dummy_prims_spiceparams, dummy_lib,
)
dummy_prims = dummy_tech.primitives


class SpicePrimsParamSpec(unittest.TestCase):
    def test_error(self):
        params = _sp.SpicePrimsParamSpec()

        # __setitem__() may not be used directly
        with self.assertRaises(TypeError):
            params["error"] = "error"

        # subcircuit_paramalias when not a subcircuit
        with self.assertRaises(ValueError):
            params.add_device_params(
                prim=dummy_prims.resistor, subcircuit_paramalias={"error": "error"},
            )

        # subcircuit_paramalias not a dict with keys == {"width", "height"}
        with self.assertRaises(ValueError):
            params.add_device_params(
                prim=dummy_prims.resistor,
                is_subcircuit=True, subcircuit_paramalias={"error": "error"},
            )

        # subcircuit_paramalias given when not a subcircuit
        with self.assertRaises(TypeError):
            params.add_device_params(
                prim=dummy_prims.resistor, sheetres=10.0,
                is_subcircuit=True, subcircuit_paramalias={"width": "w", "length": "l"},
            )


nlfab = _sp.SpiceNetlistFactory(params=dummy_prims_spiceparams)
class SpiceNetlistFactory(unittest.TestCase):
    def test_doublenet(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["nmos"], name="inst")
        ckt.new_net(name="s", external=True, childports=inst.ports["sourcedrain1"])
        ckt.new_net(name="s2", external=True, childports=inst.ports["sourcedrain1"])

        with self.assertRaises(ValueError):
            nlfab.export_circuit(ckt)

    def test_unconnected(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["nmos"], name="inst")
        ckt.new_net(name="s", external=True, childports=inst.ports["sourcedrain1"])

        with self.assertRaises(ValueError):
            nlfab.export_circuit(ckt)

    def test_mosfet(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["nmos"], name="inst")
        ckt.new_net(name="s", external=True, childports=inst.ports["sourcedrain1"])
        ckt.new_net(name="g", external=True, childports=inst.ports["gate"])
        ckt.new_net(name="d", external=True, childports=inst.ports["sourcedrain2"])
        ckt.new_net(name="b", external=True, childports=inst.ports["bulk"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test s g d b
                Minst s g d b nmos l=2.5e-07 w=3e-07
                .ends test
            """)
        )

    def test_bip(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["npn"], name="inst")
        ckt.new_net(name="c", external=True, childports=inst.ports["collector"])
        ckt.new_net(name="b", external=True, childports=inst.ports["base"])
        ckt.new_net(name="e", external=True, childports=inst.ports["emitter"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test c b e
                Qinst c b e npn
                .ends test
            """)
        )

    def test_res1(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["polyres"], name="inst")
        ckt.new_net(name="p1", external=True, childports=inst.ports["port1"])
        ckt.new_net(name="p2", external=True, childports=inst.ports["port2"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test p1 p2
                Xinst p1 p2 polyres l=2.5e-07 w=2.5e-07
                .ends test
            """)
        )

    def test_res2(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["metal2res"], name="inst")
        ckt.new_net(name="p1", external=True, childports=inst.ports["port1"])
        ckt.new_net(name="p2", external=True, childports=inst.ports["port2"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test p1 p2
                Rinst p1 p2 1.0000000000e+01 mres2 l=1e-07 w=1e-07
                .ends test
            """)
        )

        self.assertEqual(
            nlfab.export_circuit(ckt, use_semiconres=False),
            dedent("""\
                .subckt test p1 p2
                Rinst p1 p2 1.0000000000e+01
                .ends test
            """)
        )

    def test_res3(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["resistor"], name="inst")
        ckt.new_net(name="p1", external=True, childports=inst.ports["port1"])
        ckt.new_net(name="p2", external=True, childports=inst.ports["port2"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test p1 p2
                Xinst p1 p2 resistor l=3e-07 w=3e-07
                .ends test
            """)
        )

    def test_res4(self):
        params = _sp.SpicePrimsParamSpec()
        params.add_device_params(
            prim=dummy_prims["polyres"], sheetres=10.0, is_subcircuit=False,
        )
        fab = _sp.SpiceNetlistFactory(params=params)

        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(dummy_prims["polyres"], name="inst")
        ckt.new_net(name="p1", external=True, childports=inst.ports["port1"])
        ckt.new_net(name="p2", external=True, childports=inst.ports["port2"])

        self.assertEqual(
            fab.export_circuit(ckt),
            dedent("""\
                .subckt test p1 p2
                Rinst p1 p2 1.0000000000e+01
                .ends test
            """)
        )

    def test_mimcap(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(
            dummy_prims["MIMCap"], name="mim",
            width=0.5, height=0.5,
        )
        ckt.new_net(name="bot", external=True, childports=inst.ports["bottom"])
        ckt.new_net(name="top", external=True, childports=inst.ports["top"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test bot top
                Xmim top bot MIMCap w=5e-07 h=5e-07
                .ends test
            """)
        )

    def test_mimcap_nosubckt(self):
        params = _sp.SpicePrimsParamSpec()
        params.add_device_params(
            prim=dummy_prims["MIMCap"], is_subcircuit=False,
        )
        fab = _sp.SpiceNetlistFactory(params=params)

        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(
            dummy_prims["MIMCap"], name="mim",
            width=0.5, height=0.5,
        )
        ckt.new_net(name="mim_bot", external=True, childports=inst.ports["bottom"])
        ckt.new_net(name="mim_top", external=True, childports=inst.ports["top"])

        with self.assertRaises(NotImplementedError):
            fab.export_circuit(ckt)

    def test_ndiode(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(
            dummy_prims["ndiode"], name="dio",
        )
        ckt.new_net(name="ano", external=True, childports=inst.ports["anode"])
        ckt.new_net(name="cath", external=True, childports=inst.ports["cathode"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test ano cath
                Ddio ano cath ndiode area=9e-14 pj=1.2e-06
                .ends test
            """)
        )

    def test_pdiode(self):
        ckt = dummy_cktfab.new_circuit(name="test")

        inst = ckt.instantiate(
            dummy_prims["pdiode"], name="dio",
        )
        ckt.new_net(name="ano", external=True, childports=inst.ports["anode"])
        ckt.new_net(name="cath", external=True, childports=inst.ports["cathode"])

        self.assertEqual(
            nlfab.export_circuit(ckt),
            dedent("""\
                .subckt test ano cath
                Xdio ano cath pdiode w=3e-07 h=3e-07
                .ends test
            """)
        )

    def test_inst(self):
        cell = _cell.Cell(
            name="test",
            tech=dummy_tech, cktfab=dummy_cktfab, layoutfab=dummy_layoutfab,
        )
        ckt = cell.new_circuit()

        inst = ckt.instantiate(dummy_prims["polyres"], name="inst")
        ckt.new_net(name="p1", external=True, childports=inst.ports["port1"])
        ckt.new_net(name="p2", external=True, childports=inst.ports["port2"])

        ckt2 = dummy_cktfab.new_circuit(name="test2")

        inst = ckt2.instantiate(cell, name="inst")
        ckt2.new_net(name="p1", external=True, childports=inst.ports["p1"])
        ckt2.new_net(name="p2", external=True, childports=inst.ports["p2"])

        self.assertEqual(
            nlfab.export_circuit(ckt2),
            dedent("""\
                .subckt test2 p1 p2
                Xinst p1 p2 test
                .ends test2
            """)
        )

        self.assertEqual(
            nlfab.export_circuits(ckt2),
            dedent("""\
                .subckt test p1 p2
                Xinst p1 p2 polyres l=2.5e-07 w=2.5e-07
                .ends test

                .subckt test2 p1 p2
                Xinst p1 p2 test
                .ends test2
            """)
        )

    def test_lib(self):
        s_ckts = nlfab.export_circuits(
            (cell.circuit for cell in dummy_lib.cells)
        )
        self.assertEqual(
            nlfab.export_library(dummy_lib),
            f"* {dummy_lib.name}\n\n{s_ckts}\n"
        )

# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from pathlib import Path
from typing import Any
import unittest

from pdkmaster.task import OpenPDKTree
from pdkmaster.io.spice.doit import TaskManager

from ...dummy import dummy_netlistfab, dummy_lib


class TestDoit(unittest.TestCase):
    def test_tm_task(self):
        tmp = Path("/tmp/open_pdk")
        tree = OpenPDKTree(top=tmp, pdk_name="dummy")

        def cb() -> Any:
            return None

        tm = TaskManager(
            tech_cb=cb,
            lib4name_cb=(lambda _: dummy_lib),
            cell_list={"dummy": ["cell1"]},
            top_dir=tmp,
            openpdk_tree=tree,
            netlistfab_cb=(lambda: dummy_netlistfab),
        )
        self.assertEqual(tm.netlistfab, dummy_netlistfab)

        t = tm.create_export_task()

        td = tuple(t.task_func())
        self.assertEqual(td[0]["name"], "dummy")

        # Just run the export for code coverage, rightness of output is checked
        # in other unit tests
        t._gen_spice("dummy")


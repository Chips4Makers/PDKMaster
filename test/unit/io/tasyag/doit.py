# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from pathlib import Path
from typing import Any
import unittest

from pdkmaster.task import OpenPDKTree
from pdkmaster.io.tasyag.doit import TaskManager

from ...dummy import dummy_lib


class TestDoit(unittest.TestCase):
    def test_tm_error(self):
        tmp = Path("/tmp/open_pdk")
        tree = OpenPDKTree(top=tmp, pdk_name="dummy")
        spi_file = tmp.joinpath("models.spi")

        def cb() -> Any:
            return None

        tm = TaskManager(
            tech_cb=cb,
            lib4name_cb=(lambda _: dummy_lib),
            cell_list={"dummy": ["cell1"]},
            top_dir=tmp,
            openpdk_tree=tree,
        )

        t_rtl = tm.create_rtl_task(work_dir=tmp, spice_model_files=spi_file)

        with self.assertRaises(NotImplementedError):
            t_rtl._rtl_script("dummy", "chisel")

        t_liberty = tm.create_liberty_task(
            work_dir=tmp, corner_data={
                "dummy": {
                    "nom": (5.0, 25, (spi_file,))
                },
            },
        )

        with self.assertRaises(AssertionError):
            td = tuple(t_liberty.task_func())

    def test_tm_task(self):
        # We don't test coverage of running the tasks themselves as dummy libary is not
        # fit for that. We do mark these methods as 'no cover'.
        # We assume it is tested by the real PDKs.
        from dataclasses import dataclass

        @dataclass
        class MyTask:
            name: str

        tmp = Path("/tmp/open_pdk")
        tree = OpenPDKTree(top=tmp, pdk_name="dummy")
        spi_file = tmp.joinpath("models.spi")

        def cb() -> Any:
            return None

        tm = TaskManager(
            tech_cb=cb,
            lib4name_cb=(lambda _: dummy_lib),
            cell_list={"stdcelltest": ["Gallery", "inv"]},
            top_dir=tmp,
            openpdk_tree=tree,
            spice_models_dir=tmp.joinpath("models"),
        )

        self.assertEqual(tm.out_dir_drc, tmp.joinpath("drc"))
        self.assertEqual(tm.out_dir_lvs, tmp.joinpath("lvs"))

        t_rtl = tm.create_rtl_task(
            work_dir=tmp, spice_model_files=spi_file, override_dir=tmp.joinpath("override"),
        )

        tds = tuple(t_rtl.task_func())
        self.assertEqual(
            set(td["name"] for td in tds),
            {"verilog", "vhdl", "stdcelltest", "stdcelltest:verilog", "stdcelltest:vhdl"},
        )
        self.assertEqual(
            tds[0]["title"](MyTask("rtl:dummy:verilog")),
            "Creating dummy:verilog files",
        )

        t_liberty = tm.create_liberty_task(
            work_dir=tmp, corner_data={
                "stdcelltest": {
                    "nom": (5.0, 25, (spi_file,))
                },
            },
        )

        tds = tuple(t_liberty.task_func())
        self.assertEqual(set(td["name"] for td in tds), {"stdcelltest_nom"})
        self.assertEqual(
            tds[0]["title"](MyTask("liberty:dummy_nom")),
            "Creating liberty files for library dummy, corner nom",
        )


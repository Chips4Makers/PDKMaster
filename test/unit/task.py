# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import os
from pathlib import Path
from typing import Callable, Dict, Collection, Optional
import unittest

from pdkmaster.task import *
from pdkmaster.technology.technology_ import Technology

from .dummy import dummy_tech, dummy_lib


class TestOpenPDKTree(unittest.TestCase):
    def test_get_var_env(self):
        os.environ["MYTESTVAR"] = "test"
        self.assertIsNone(get_var_env("_nonexist5_"))
        self.assertEqual(get_var_env("_nonexist5_", default="1"), "1")
        self.assertEqual(get_var_env("mytestvar"), "test")

    def test_tree(self):
        tmp = Path("/tmp")
        pdk = "dummy"
        tree = OpenPDKTree(top=tmp, pdk_name=pdk)

        self.assertEqual(tree.top, tmp)
        self.assertEqual(tree.pdk_name, pdk)
        self.assertEqual(tree.pdk_dir, tmp.joinpath(pdk))
        self.assertEqual(tree.tool_dir(tool_name="test"), tmp.joinpath(pdk, "libs.tech", "test"))
        self.assertEqual(
            tree.views_dir(lib_name="lib", view_name="gds"),
            tmp.joinpath(pdk, "libs.ref", "lib", "gds"),
        )

        td = tree.task_func()
        self.assertEqual(td["targets"], (tmp,))
        self.assertEqual(td["clean"], ("rm -fr /tmp",))


class TestTaskManager(unittest.TestCase):
    def test_taskmanager(self):
        def get_tech():
            return dummy_tech
        tmp = Path("/tmp")
        tree = OpenPDKTree(top=tmp.joinpath("open_pdk"), pdk_name="dummy")

        class MyTaskManager(TaskManager):
            def __init__(self, *,
                tech_cb: Callable[[], Technology],
                cell_list: Dict[str, Collection[str]],
                top_dir: Path,
                openpdk_tree: OpenPDKTree,
            ) -> None:
                super().__init__(
                    tech_cb=tech_cb,
                    lib4name_cb=lambda _: dummy_lib,
                    cell_list=cell_list, top_dir=top_dir,
                    openpdk_tree=openpdk_tree,
                )
        tm = MyTaskManager(
            tech_cb=get_tech, cell_list={dummy_lib.name: ["cell1"]}, top_dir=tmp, openpdk_tree=tree,
        )

        self.assertEqual(tm.tech, dummy_tech)
        self.assertEqual(tm.pdk_name, tree.pdk_name)
        self.assertEqual(tm.cells4lib(dummy_lib), ["cell1"])
        self.assertEqual(tm.cells4lib(dummy_lib.name), ["cell1"])

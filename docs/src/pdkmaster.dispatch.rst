pdkmaster.dispatch
==================

.. automodule:: pdkmaster.dispatch
    :members:
    :undoc-members:
    :show-inheritance:

pdkmaster.dispatch.shape
------------------------

.. automodule:: pdkmaster.dispatch.shape
    :members:
    :undoc-members:
    :show-inheritance:

pdkmaster.dispatch.primitive
----------------------------

.. automodule:: pdkmaster.dispatch.primitive
    :members:
    :undoc-members:
    :show-inheritance:
